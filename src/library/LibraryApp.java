package library;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class LibraryApp {

    public Database database = new Database();

    public void run() {
        login();
    }

    private void login() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter clent name: ");
        String name = scan.nextLine();
        System.out.println("Enter client id: ");
        int id = scan.nextInt();
        String result = database.signIn(name, id);
        if (result == "Ok") {
            System.out.println("Logged in, welcome " + database.getCurrentClientName());
            mainMenu();
        } else {
            System.out.println(result);
        }
    }

    private void displayMainMenu() {
        System.out.println("LIBRARY APP MAIN MENU" + '\n');

        System.out.println("1. Browse book catalog");
        System.out.println("2. Search for book");
        System.out.println("3. Return book");
        System.out.println("4. Donate book");
    }

    private void mainMenu() {
        Scanner scan = new Scanner(System.in);
        boolean exit = false;
        displayMainMenu();
        while (!exit) {
            System.out.println("Choose option (exit to quit): ");
            String choice = scan.nextLine();
            switch (choice) {
                case "1":
                    System.out.println("BOOK REPOSITORY CONTENTS" + '\n');
                    database.browseBookRepository();
                    System.out.println("Press enter to continue...");
                    scan.nextLine();
                    displayMainMenu();
                    break;
                case "2":
                    searchMenu();
                    displayMainMenu();
                    break;
                case "3":
                    returnBookMenu();
                    displayMainMenu();
                    break;
                case "4":
                    bookDonation();
                    displayMainMenu();
                    break;
                case "exit":
                    exit = true;
                    break;
                default:
                    System.out.println("Invalid option");
                    break;
            }

        }
    }

    private void searchMenu() {
        System.out.println("Search book repository by:" + '\n');

        System.out.println("1. Title");
        System.out.println("2. Author");
        System.out.println("3. Year");
        System.out.println("4. ISBN");

        Boolean back = false;
        Scanner scan = new Scanner(System.in);
        while (!back) {
            System.out.println("Choose option ('back' to go back to main): ");
            String choice = scan.nextLine();
            switch (choice) {
                case "1":
                    Scanner scanTitle = new Scanner(System.in);
                    System.out.println("Enter title to search: ");
                    String title = scanTitle.nextLine();
                    database.searchTitle(title);
                    break;
                case "2":
                    Scanner scanAuthor = new Scanner(System.in);
                    System.out.println("Enter author's name to search: ");
                    String author = scanAuthor.nextLine();
                    database.searchAuthor(author);
                    break;
                case "3":
                    Scanner scanYear = new Scanner(System.in);
                    System.out.println("Enter year to search: ");
                    int year = scanYear.nextInt();
                    database.searchYear(year);
                    break;
                case "4":
                    Scanner scanISBN = new Scanner(System.in);
                    System.out.println("Enter first digits of ISBN to search: ");
                    String isbn = scanISBN.nextLine();
                    database.searchISBN(isbn);
                    break;
                case "back":
                    back = true;
                    break;
                default:
                    System.out.println("Invalid option");
                    break;
            }

        }
    }

    private void returnBookMenu() {
        ArrayList<Book> bookList = new ArrayList<>(database.getClientBooks());
        System.out.println("BOOKS BORROWED BY CLIENT: " + database.getCurrentClientName());
        database.enumerate(bookList);
        database.returnPrompt(bookList);
    }

    private void bookDonation() {
        String donationTitle, donationAuthor, donationISBN;
        int donationYear = 0;
        System.out.println("BOOK DONATIONS" + '\n');
        Scanner scan = new Scanner(System.in);

        System.out.println("Enter book ISBN:");
        donationISBN = scan.nextLine();

        if (database.checkISBN(donationISBN)) {
            System.out.println("Enter book title:");
            donationTitle = scan.nextLine();

            System.out.println("Enter book author:");
            donationAuthor = scan.nextLine();

            System.out.println("Enter book year:");
            boolean correctYear = false;
            do {
                try {
                    donationYear = scan.nextInt();
                    correctYear = true;
                } catch (InputMismatchException e) {
                    System.out.println("The year entered is not a number. " + '\n' + "Enter another number: ");
                    scan.next();
                }
            } while (correctYear == false);

            scan.nextLine();
            Book donation = new Book(donationTitle, donationAuthor, donationISBN, donationYear);
            database.donateBook(donation);
            System.out.println("Donation successfully added. Thank you!");
        } else {
            System.out.println("The library already owns this book - donation was not accepted.");
        }
    }

}


