package library;

import java.util.*;


public class Database {

    private Map<Client, ArrayList<Book>> clients = new HashMap<>();
    private BookRepository repository;
    private Client currentClient;
    private String currentClientName = "anonymous";
    private Integer currentClientID;


    public Database() {
        Client client1 = new Client("Robert", 14);
        Client client2 = new Client("Kate", 10);
        Client client3 = new Client("Stephen", 44);

        clients.put(client1, new ArrayList<>());
        clients.put(client2, new ArrayList<>());
        clients.put(client3, new ArrayList<>());
    }

    public String signIn(String name, int id) {
        repository = new BookRepository();
        String message = null;
        if (!repository.loadDatabaseFile()) {
            message = "File not found.";
        } else if (!repository.readDatabaseFile()) {
        message = "File is empty.";
        } else {
            Client client = new Client(name, id);
            if (clients.containsKey(client)) {
                setCurrentClient(name, id);
                message = "Ok";
            } else {
                message = "Client does not exist.";
            }
        }return message;

    }

    public void enumerate(ArrayList<Book> bookList) {
        for (Book book : bookList) {
            System.out.println((bookList.indexOf(book) + 1) + ". " + book.toString());
        }
    }

    public void browseBookRepository() {
        repository.browse();
    }

    public void searchTitle(String title) {
        ArrayList<Book> bookList = repository.searchByTitle(title);
        if (bookList.isEmpty()) {
            System.out.println("No books with this phrase in the title were found.");
        } else {
            enumerate(bookList);
            borrowPrompt(bookList);
        }
    }

    public void searchAuthor(String author) {
        ArrayList<Book> bookList = repository.searchByAuthor(author);
        if (bookList.isEmpty()) {
            System.out.println("No books by this author were found");
        } else {
            enumerate(bookList);
            borrowPrompt(bookList);
        }
    }

    public void searchYear(int year) {
        ArrayList<Book> bookList = repository.searchByYear(year);
        if (bookList.isEmpty()) {
            System.out.println("No books from this year were found.");
        } else {
            enumerate(bookList);
            borrowPrompt(bookList);
        }
    }

    public void searchISBN(String isbn) {
        ArrayList<Book> bookList = repository.searchByISBN(isbn);
        if (bookList.isEmpty()) {
            System.out.println("No books from this year were found.");
        } else {
            enumerate(bookList);
            borrowPrompt(bookList);
        }
    }

    private void borrowPrompt(ArrayList<Book> list) {
        Scanner bookNo = new Scanner(System.in);
        if (list.isEmpty()) return;
        else {
            while (true) {
                try {
                    System.out.println("Enter book number to borrow ('0' to return): ");
                    int bookNumber = bookNo.nextInt();
                    if (bookNumber > list.size() + 1 || bookNumber < 0) {
                        System.out.println("This number is not on the list.");
                        borrowPrompt(list);
                    } else if (bookNumber == 0) return;
                    else {
                        borrowBook(list.get(bookNumber - 1));
                        break;
                    }
                } catch (InputMismatchException e) {
                    System.out.println("Please enter a correct number.");
                    bookNo.next();
                }
            }
        }
    }

    public void returnPrompt(ArrayList<Book> list) {
        Scanner bookNo = new Scanner(System.in);
        if (getClientBooks().isEmpty()) {
            System.out.println("You have no books to return.");
        } else {
            while (true) {
                try {
                    System.out.println("Enter book number to return ('0' to go back): ");
                    int bookNumber = bookNo.nextInt();
                    if (bookNumber > list.size() + 1 || bookNumber < 0) {
                        System.out.println("This number is not on the list.");
                        returnPrompt(list);
                    } else if (bookNumber == 0) return;
                    else {
                        returnBook(list.get(bookNumber - 1));
                        break;
                    }
                } catch (InputMismatchException e) {
                    System.out.println("Please enter a correct number.");
                    bookNo.next();

                }
            }
        }
    }

    private void borrowBook(Book book) {
        Optional isBorrowed = Optional.ofNullable(repository.borrowCopy(book));
        if (isBorrowed.isPresent()) {
            clients.get(currentClient).add(book);
            System.out.println(book.getTitle() + " was borrowed to " + currentClientName + ".");
        } else {
            System.out.println("Sorry, this book is not available.");
        }
    }

    private void returnBook(Book book) {
        List<Book> clientBooks = new ArrayList<>(getClientBooks());
        boolean clientHas = clientBooks.contains(book);
        if (clientHas) {
            clientBooks.remove(book);
            repository.returnCopy(book);
            System.out.println(book.getTitle() + " was returned to library by " + currentClientName);
        } else {
            System.out.println("You don't have this book!");
        }
    }

    public ArrayList<Book> getClientBooks() {
        return clients.get(currentClient);
    }

    public void donateBook(Book donation) {
        repository.addDonation(donation);
    }

    public boolean checkISBN(String isbn) {
        return repository.checkISBN(isbn);
    }

    public void setCurrentClient(String name, Integer id) {
        Client client = new Client(name, id);
        currentClient = client;
        currentClientName = name;
        currentClientID = id;

    }

    public String getCurrentClientName() {
        return currentClientName;
    }

}