package library;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class BookRepository {

    private Map<Book, Integer> bookRepository = new HashMap<>();
    private BufferedReader databaseReader = null;


    public BookRepository() {
    }

    public boolean loadDatabaseFile() {
        boolean result = true;
        try {
            databaseReader = new BufferedReader(new FileReader("database.txt"));
        } catch (FileNotFoundException e) {
            result = false;
        }

        return result;
    }
    public boolean readDatabaseFile() {
        boolean result = true;
        String line;
        String[] bookData;
        try {
            line  = databaseReader.readLine();
            while (line!= null) {
                line = databaseReader.readLine();
                bookData = line.split(";");
                bookRepository.put(new Book(bookData[0], bookData[1], bookData[2], Integer.parseInt(bookData[3])), 1);
            }
        } catch (IOException e) {
            result = false;
        }catch (NullPointerException e){
            System.out.println("Empty file");
            result = false;
        }
        return result;
    }


    public void browse() {
        List<Book> bookCatalog = new ArrayList<>(bookRepository.keySet());
        for (Book book : bookCatalog) {
            String entry = (bookCatalog.indexOf(book) + 1) +
                    ". " + book.toString() +
                    ";  available: " + bookRepository.get(book);
            System.out.println(entry);
        }
    }

    public ArrayList<Book> searchByTitle(String titleSearched) {
        ArrayList<Book> booksFound = new ArrayList<>();
        for (Book book : bookRepository.keySet()) {
            String title = book.getTitle().toLowerCase();
            if (title.contains(titleSearched.toLowerCase())) {
                booksFound.add(book);
            }
        }
        return booksFound;
    }

    public ArrayList<Book> searchByAuthor(String authorSearched) {
        ArrayList<Book> booksFound = new ArrayList<>();
        for (Book book : bookRepository.keySet()) {
            String author = book.getAuthor().toLowerCase();
            if (author.contains(authorSearched.toLowerCase())) {
                booksFound.add(book);
            }
        }
        return booksFound;
    }

    public ArrayList<Book> searchByYear(int yearSearched) {
        ArrayList<Book> booksFound = new ArrayList<>();
        for (Book book : bookRepository.keySet()) {
            int year = book.getYear();
            if (yearSearched == year) {
                booksFound.add(book);
            }
        }
        return booksFound;
    }

    public ArrayList<Book> searchByISBN(String ISBNsearched) {
        ArrayList<Book> booksFound = new ArrayList<>();
        for (Book book : bookRepository.keySet()) {
            String isbn = book.getISBN();
            if (isbn.startsWith(ISBNsearched)) {
                booksFound.add(book);
            }
        }
        return booksFound;
    }

    public boolean checkISBN(String isbnCompared) {
        boolean found = true;
        for (Book book : bookRepository.keySet()) {
            if (book.getISBN().equals(isbnCompared)) {
                found = false;
            }
        }
        return found;
    }


    private Boolean isAvailable(Book book) {
        int booksNo = bookRepository.get(book);
        Boolean isAvailable;
        if (booksNo > 0) {
            isAvailable = true;
        } else {
            isAvailable = false;
        }
        return isAvailable;
    }

    public Book borrowCopy(Book book) {
        if (isAvailable(book)) {
            bookRepository.put(book, (bookRepository.get(book) - 1));
            return book;
        } else {
            return null;
        }
    }

    public void returnCopy(Book book) {
        bookRepository.put(book, (bookRepository.get(book) + 1));
    }


    public void addDonation(Book donation) {

        bookRepository.put(donation, 1);
        ;
    }
}