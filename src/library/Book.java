package library;

import java.util.Objects;

public class Book {
    private String title;
    private String author;
    private String ISBN;
    private Integer year;

    public Book(String title, String author, String ISBN, Integer year) {
        this.title = title;
        this.author = author;
        this.ISBN = ISBN;
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getISBN() {
        return ISBN;
    }

    public Integer getYear() {
        return year;
    }

    @Override
    public String toString() {
        return title +
                ";  " + author +
                ";  " + year +
                ";  ISBN: " + ISBN;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return Objects.equals(title, book.title) &&
                Objects.equals(author, book.author) &&
                Objects.equals(year, book.year);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, author, ISBN, year);
    }
}

