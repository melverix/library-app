package library;


import java.util.ArrayList;
import java.util.Objects;

public class Client {

    private ArrayList<Book> bookList;
    private String name;
    private Integer id;

    public Client(String name, Integer id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return Objects.equals(bookList, client.bookList) &&
                Objects.equals(name, client.name) &&
                Objects.equals(id, client.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bookList, name, id);
    }
}
